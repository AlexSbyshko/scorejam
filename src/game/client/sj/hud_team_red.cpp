//========= Copyright Valve Corporation, All rights reserved. ============//
//
// Purpose: 
//
// $NoKeywords: $
//
//=============================================================================//
//
// Health.cpp
//
// implementation of CHudHealth class
//
#include "cbase.h"
#include "hud.h"
#include "hud_team_red.h"
#include "hud_macros.h"
#include "view.h"

#include "iclientmode.h"

#include <KeyValues.h>
#include <vgui/ISurface.h>
#include <vgui/ISystem.h>
#include <vgui_controls/AnimationController.h>

#include <vgui/ILocalize.h>

using namespace vgui;

#include "hudelement.h"
#include "hud_basetimer.h"
#include "hl2mp_gamerules.h"
#include "c_playerresource.h"

#include "convar.h"

// memdbgon must be the last include file in a .cpp file!!!
#include "tier0/memdbgon.h"

//-----------------------------------------------------------------------------
// Purpose: Health panel
//-----------------------------------------------------------------------------


DECLARE_HUDELEMENT( CHudTeamRed );

//-----------------------------------------------------------------------------
// Purpose: Constructor
//-----------------------------------------------------------------------------
CHudTeamRed::CHudTeamRed( const char *pElementName ) : CHudElement( pElementName ), vgui::Panel( NULL, "HudTeamRed" )
{
	vgui::Panel *pParent = g_pClientMode->GetViewport( );
	SetParent( pParent );
}

//-----------------------------------------------------------------------------
// Purpose: 
//-----------------------------------------------------------------------------
void CHudTeamRed::Init( )
{
	Reset();
}

//-----------------------------------------------------------------------------
// Purpose: 
//-----------------------------------------------------------------------------
void CHudTeamRed::Reset( )
{
}

//-----------------------------------------------------------------------------
// Purpose: 
//-----------------------------------------------------------------------------
void CHudTeamRed::VidInit( )
{
	Reset();
}

//-----------------------------------------------------------------------------
// Purpose: 
//-----------------------------------------------------------------------------
void CHudTeamRed::OnThink( )
{
}

void CHudTeamRed::Paint()
{
	if ( g_PR == NULL )
	{
		return;
	}
	surface()->DrawSetTextFont( m_hTextFont );
	Color color = g_PR->GetTeamColor( TEAM_RED );
	surface( )->DrawSetTextColor( color );

	int x, y, wide, tall;
	this->GetBounds( x, y, wide, tall );

	surface( )->DrawSetTextPos( 3, 2 );

	wchar_t teamNameUnicode[MAX_TEAMNAME_LENGTH];
	const char *tempName = g_PR->GetTeamName( TEAM_RED );
	g_pVGuiLocalize->ConvertANSIToUnicode( tempName, teamNameUnicode, sizeof(teamNameUnicode) );
	surface()->DrawPrintText( teamNameUnicode, wcslen( teamNameUnicode ) );
}