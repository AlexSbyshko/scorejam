#ifndef SJ_GAMERULES_H
#define SJ_GAMERULES_H
#pragma once

#include "gamerules.h"
#include "teamplay_gamerules.h"

#ifndef CLIENT_DLL
#include "sj_player.h"
#include "sj_ball.h"
#endif

#ifdef CLIENT_DLL
#define CSJRules C_SJRules
#define CSJGameRulesProxy C_SJGameRulesProxy
#endif

enum
{
	TEAM_1 = 2,
	TEAM_2 = 3,
};

extern color32 cSJTeamColors[];

class CSJGameRulesProxy : public CGameRulesProxy
{
public:
	DECLARE_CLASS( CSJGameRulesProxy, CGameRulesProxy );
	DECLARE_NETWORKCLASS();
};


class CSJRules : public CTeamplayRules
{
public:
	DECLARE_CLASS( CSJRules, CTeamplayRules );

#ifdef CLIENT_DLL

	DECLARE_CLIENTCLASS_NOBASE(); // This makes datatables able to access our private vars.

#else

	DECLARE_SERVERCLASS_NOBASE(); // This makes datatables able to access our private vars.
#endif

	CSJRules();
	virtual ~CSJRules();

	virtual void Think( void );
	
	void StartMatch( void );
	void SpawnBall( void );

#ifndef CLIENT_DLL

	bool IsPlayerInOffside( CSJ_Player *pPlayer );
	void OnPlayerKickedBall( CSJ_Player *pPlayer );

#endif

private:
	CNetworkVar( float, m_flMatchStartTime );
};

inline CSJRules* SJRules()
{
	return static_cast<CSJRules*>(g_pGameRules);
}

inline int GetOpponentTeamNumber( int teamNumber )
{
	if ( teamNumber == TEAM_1 )
	{
		return TEAM_2;
	}
	else if ( teamNumber == TEAM_2 )
	{
		return TEAM_1;
	}
	else
	{
		return TEAM_UNASSIGNED;
	}
}

#endif //SJ_GAMERULES_H
