//========= Copyright Valve Corporation, All rights reserved. ============//
//
// Purpose:		Player for SJ.
//
//=============================================================================//

#include "cbase.h"
#include "sj_player.h"
#include "sj_gamerules.h"



LINK_ENTITY_TO_CLASS( sjplayer, CSJ_Player );

LINK_ENTITY_TO_CLASS( info_player_team1, CPointEntity );
LINK_ENTITY_TO_CLASS( info_player_team2, CPointEntity );

IMPLEMENT_SERVERCLASS_ST( CSJ_Player, DT_SJ_Player )
SendPropAngle( SENDINFO_VECTORELEM( m_angEyeAngles, 0 ), 11, SPROP_CHANGES_OFTEN ),
SendPropAngle( SENDINFO_VECTORELEM( m_angEyeAngles, 1 ), 11, SPROP_CHANGES_OFTEN ),
SendPropEHandle( SENDINFO( m_hRagdoll ) ),
SendPropInt( SENDINFO( m_iSpawnInterpCounter ), 4 ),
SendPropInt( SENDINFO( m_iPlayerSoundType ), 3 ),

SendPropExclude( "DT_BaseAnimating", "m_flPoseParameter" ),
SendPropExclude( "DT_BaseFlex", "m_viewtarget" ),

END_SEND_TABLE()

BEGIN_DATADESC( CSJ_Player )
END_DATADESC()

CSJ_Player::CSJ_Player()
{
	m_angEyeAngles.Init();

	m_iSpawnInterpCounter = 0;
	m_bBallCharging = false;
	m_flStartChargeTime = 0;
}

CSJ_Player::~CSJ_Player()
{	
}

void CSJ_Player::Precache( void )
{
	BaseClass::Precache();
	PrecacheModel( "models/police.mdl" );
}

void CSJ_Player::Spawn( void )
{
	BaseClass::Spawn();
	SetModel( "models/police.mdl" );
}

CBaseEntity* CSJ_Player::EntSelectSpawnPoint( void )
{
	const char *pSpawnpointName = "info_player_team1";
	CBaseEntity *pSpot = NULL;
	pSpot = gEntList.FindEntityByClassname( pSpot, pSpawnpointName );
	return pSpot;
}

class CTraceFilterKickInterferences : public ITraceFilter
{
public:
	virtual bool ShouldHitEntity( IHandleEntity *pHandleEntity, int contentsMask )
	{
		CBaseEntity *pEntity = EntityFromEntityHandle( pHandleEntity );

		CSJBallEntity *pBall = FindBall();
		int ballIndex = (pBall == NULL) ? -1 : pBall->entindex();

		return !pEntity->IsPlayer()
			&& pEntity->entindex() != ballIndex
			&& !FStrEq( "func_goal_red", pEntity->GetClassname() )
			&& !FStrEq( "func_goal_blue", pEntity->GetClassname() );
	}

	virtual TraceType_t	GetTraceType() const
	{
		return TRACE_EVERYTHING;
	}
};

bool CSJ_Player::IsInterferenceToKick()
{
	Vector playerOrigin = GetAbsOrigin();
	QAngle playerAngles = EyeAngles();

	vec_t radians = playerAngles[1] * M_PI / 180;
	float sine = sin( radians );
	float cosine = cos( radians );

	float BALL_RADIUS = 12.0f;
	float BALL_KICK_HEIGHT = 30.0f;
	float KICK_DISTANCE = 55.0f;

	Vector leftBottomOrigin;
	leftBottomOrigin[0] = playerOrigin[0] - cosine * BALL_RADIUS;
	leftBottomOrigin[1] = playerOrigin[1] - sine * BALL_RADIUS;
	leftBottomOrigin[2] = playerOrigin[2] + BALL_KICK_HEIGHT - BALL_RADIUS;

	Vector startOriginAddtitions;
	startOriginAddtitions[0] = cosine * BALL_RADIUS;
	startOriginAddtitions[1] = sine * BALL_RADIUS;
	startOriginAddtitions[2] = BALL_RADIUS;

	Vector testOriginAdditions;
	testOriginAdditions[0] = cosine * (KICK_DISTANCE + BALL_RADIUS);
	testOriginAdditions[1] = sine * (KICK_DISTANCE + BALL_RADIUS);
	testOriginAdditions[2] = 0.0;

	CTraceFilterKickInterferences traceFilter;

	Vector startOrigin;
	Vector testOrigin;
	for ( int x = 0; x < 3; x++ )
	{
		for ( int y = 0; y < 3; y++ )
		{
			for ( int z = 0; z < 3; z++ )
			{
				startOrigin[0] = leftBottomOrigin[0] + x * startOriginAddtitions[0];
				startOrigin[1] = leftBottomOrigin[1] + y * startOriginAddtitions[1];
				startOrigin[2] = leftBottomOrigin[2] + z * startOriginAddtitions[2];
				for ( int j = 0; j < 3; j++ )
				{
					testOrigin[j] = startOrigin[j] + testOriginAdditions[j];
				}

				Ray_t ray;
				ray.Init( startOrigin, testOrigin );

				trace_t tr;
				enginetrace->TraceRay( ray, MASK_SOLID, &traceFilter, &tr );

				if ( tr.DidHit() )
				{
					return true;
				}
			}
		}
	}

	return false;
}

void CSJ_Player::OnBallReceiving()
{
	color32 color = cSJTeamColors[GetTeamNumber()];
	color.a = 128;
	UTIL_ScreenFade( this, color, 0.1, 0.0, FFADE_PURGE | FFADE_OUT );

	CSJBallEntity *pBall = FindBall();
	if ( m_bBallCharging && pBall != NULL )
	{
		float CHARGE_MAX_TIME = 1.0;
		float flKickStrengthMultiplier = (gpGlobals->curtime - m_flStartChargeTime) / CHARGE_MAX_TIME;
		flKickStrengthMultiplier = (flKickStrengthMultiplier > 1.0) ? 1.0 : flKickStrengthMultiplier;

		float MIN_KICK_STRENGTH = 300.0;
		float MAX_KICK_STRENGTH = 1000.0;

		float flKickStrength = MIN_KICK_STRENGTH + (MAX_KICK_STRENGTH - MIN_KICK_STRENGTH) * flKickStrengthMultiplier;

		char szReturnString[512];
		Q_snprintf( szReturnString, sizeof( szReturnString ), "Throw! ( %f%% )", flKickStrengthMultiplier * 100 );
		//UTIL_SayText(szReturnString, this);
		pBall->Kick( this, flKickStrength, m_iBallCurveState, QAngle( 0, 0, 0 ) );
		m_iBallCurveState = 0;
	}
	else
	{
		EmitSound( "PropJeep.AmmoOpen" );
	}
}