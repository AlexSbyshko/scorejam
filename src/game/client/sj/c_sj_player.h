//========= Copyright Valve Corporation, All rights reserved. ============//
//
// Purpose: 
//
// $NoKeywords: $
//
//=============================================================================//
#ifndef SJ_PLAYER_H
#define SJ_PLAYER_H
#pragma once

class C_SJ_Player;
#include "c_basehlplayer.h"

//=============================================================================
// >> SJ_Player
//=============================================================================
class C_SJ_Player : public C_BaseHLPlayer
{
public:
	DECLARE_CLASS( C_SJ_Player, C_BaseHLPlayer );

	DECLARE_CLIENTCLASS();
	DECLARE_PREDICTABLE();
	DECLARE_INTERPOLATION();

	C_SJ_Player();
	~C_SJ_Player( void );

private:
	QAngle	m_angEyeAngles;

	EHANDLE	m_hRagdoll;

	int	  m_iSpawnInterpCounter;

	int	  m_iPlayerSoundType;

	bool m_fIsWalking;
};

#endif //SJ_PLAYER_H
