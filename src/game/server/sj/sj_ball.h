#ifndef BALL_H
#define BALL_H
#pragma once

#include "basegrenade_shared.h"
#include "SpriteTrail.h"

class CSJBallEntity : public CBaseGrenade
{
public:
	DECLARE_CLASS(CSJBallEntity, CBaseGrenade);
	DECLARE_DATADESC();
 
	CSJBallEntity()
	{
		m_bActive = false;
	}

	bool IsBall() const { return true; }
 
	void Spawn( );
	virtual void Precache( ); 
	void MoveThink( );
	void SJBallTouch( CBaseEntity *pOther );
	void TeleportToPlayerFront( CBasePlayer *pPlayer );
	void Kick( CBasePlayer *pPlayer, float flSrength, int direction, QAngle spinAngle );
	void GoToOpponent( );
	bool IsFree( );
	void Clear();
	void SetOwnerPlayer( CBasePlayer *pPlayer );
	void SetHolderPlayer( CBasePlayer *pPlayer );
	CBasePlayer *GetOwnerPlayer();

	void SetFree();
	void SetNotFree();
	void ChangeTeam( int iTeamNum );
	void BlowUp();
 
private:
 
	bool m_bFree;
	bool m_bActive;
	bool m_bBlown;
	float m_flNextChangeTime;
	float m_nextCurveTime;
	CBasePlayer *m_pOwnerPlayer;
	CSpriteTrail *m_pGlowTrail;
	CSprite *m_pGlow;
	int m_iDirection;
	int m_iCurvesRemain;
	QAngle m_vecSpinDirection;
};

extern const char * BALL_MODEL;

inline CSJBallEntity *FindBall()
{
	return (CSJBallEntity *)gEntList.FindEntityByClassname(NULL, "ball");
}

inline bool IsBall(CBaseEntity *pEntity)
{
	return (dynamic_cast<CSJBallEntity *>(pEntity) != NULL);
}

inline CSJBallEntity *ToBall(CBaseEntity *pEntity)
{
	return dynamic_cast<CSJBallEntity *>(pEntity);
}


#endif //BALL_H
