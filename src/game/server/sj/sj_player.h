//========= Copyright Valve Corporation, All rights reserved. ============//
//
// Purpose: 
//
// $NoKeywords: $
//
//=============================================================================//
#ifndef SJ_PLAYER_H
#define SJ_PLAYER_H
#pragma once

class CSJ_Player;

#include "basemultiplayerplayer.h"
#include "hl2_playerlocaldata.h"
#include "hl2_player.h"

//=============================================================================
// >> SJ_Player
//=============================================================================
class CSJ_Player : public CHL2_Player
{
public:
	DECLARE_CLASS( CSJ_Player, CHL2_Player );

	CSJ_Player();
	~CSJ_Player( void );

	static CSJ_Player *CreatePlayer( const char *className, edict_t *ed )
	{
		CSJ_Player::s_PlayerEdict = ed;
		return (CSJ_Player*)CreateEntityByName( className );
	}

	DECLARE_SERVERCLASS();
	DECLARE_DATADESC();

	virtual void Precache( void );
	virtual void Spawn( void );
	virtual CBaseEntity* EntSelectSpawnPoint( void );


	bool IsInterferenceToKick( void );
	void OnBallReceiving( void );

	// Tracks our ragdoll entity.
	CNetworkHandle( CBaseEntity, m_hRagdoll );	// networked entity handle 

private:
	CNetworkQAngle( m_angEyeAngles );
	CNetworkVar( int, m_iSpawnInterpCounter );
	CNetworkVar( int, m_iPlayerSoundType );

	bool m_bBallCharging;
	int m_iBallCurveState;
	float m_flStartChargeTime;
};

inline CSJ_Player *ToSJPlayer( CBaseEntity *pEntity )
{
	if ( !pEntity || !pEntity->IsPlayer() )
	{
		return NULL;
	}

	return dynamic_cast<CSJ_Player*>(pEntity);
}

#endif //SJ_PLAYER_H
