#include "cbase.h"
#include "triggers.h"
#include "func_out.h"
#include "hl2mp_gamerules.h"

LINK_ENTITY_TO_CLASS( func_out, CFuncOutEntity );
 
BEGIN_DATADESC( CFuncOutEntity )
 
	DEFINE_ENTITYFUNC( Touch ),
 
END_DATADESC()

void CFuncOutEntity::Spawn()
{
	SetTouch( &CFuncOutEntity::Touch ); 
	SetSolid( SOLID_VPHYSICS );
	SetMoveType( MOVETYPE_NONE );
	SetModel( STRING( GetModelName() ) );
}

void CFuncOutEntity::Touch( CBaseEntity *pOther )
{
	if ( IsBall(pOther) )
	{
		UTIL_SayTextAll("OUT!");
		CSJBallEntity *pBall = ToBall(pOther);
		if (pBall)
		{
			pBall->GoToOpponent();
		}
	}
}