//========= Copyright Valve Corporation, All rights reserved. ============//
//
// Purpose: 
//
// $NoKeywords: $
//=============================================================================//
#if !defined( HUD_TEAM_RED_H )
#define HUD_TEAM_RED_H
#ifdef _WIN32
#pragma once
#endif

#include "hudelement.h"
#include "hud_numericdisplay.h"
#include <vgui_controls/Panel.h>

//-----------------------------------------------------------------------------
// Purpose: Red Team panel
//-----------------------------------------------------------------------------
class CHudTeamRed : public CHudElement, public vgui::Panel
{
	DECLARE_CLASS_SIMPLE( CHudTeamRed, vgui::Panel );

public:
	CHudTeamRed( const char *pElementName );
	virtual void Init( void );
	virtual void VidInit( void );
	virtual void Reset( void );
	virtual void OnThink( );

protected:
	virtual void Paint();

private:

	CPanelAnimationVar( vgui::HFont, m_hTextFont, "TextFont", "HudHintTextLarge" );
};

#endif // HUD_TEAM_RED_H
