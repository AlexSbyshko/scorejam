//========= Copyright Valve Corporation, All rights reserved. ============//
//
// Purpose: 
//
// $NoKeywords: $
//
//=============================================================================//
//
// Health.cpp
//
// implementation of CHudHealth class
//
#include "cbase.h"
#include "hud.h"
#include "hud_macros.h"
#include "view.h"

#include "iclientmode.h"

#include <KeyValues.h>
#include <vgui/ISurface.h>
#include <vgui/ISystem.h>
#include <vgui_controls/AnimationController.h>

#include <vgui/ILocalize.h>

using namespace vgui;

#include "hudelement.h"
#include "hud_match_state.h"
#include "hl2mp_gamerules.h"
#include "c_playerresource.h"

#include "convar.h"

// memdbgon must be the last include file in a .cpp file!!!
#include "tier0/memdbgon.h"


DECLARE_HUDELEMENT( CHudMatchState );
DECLARE_HUD_MESSAGE( CHudMatchState, HudTime );

//-----------------------------------------------------------------------------
// Purpose: Constructor
//-----------------------------------------------------------------------------
CHudMatchState::CHudMatchState( const char *pElementName ) : CHudElement( pElementName ), vgui::Panel( NULL, "HudMatchState" )
{
	vgui::Panel *pParent = g_pClientMode->GetViewport( );
	SetParent( pParent );
}

//-----------------------------------------------------------------------------
// Purpose: 
//-----------------------------------------------------------------------------
void CHudMatchState::Init( )
{
	HOOK_HUD_MESSAGE( CHudMatchState, HudTime );
	m_iMatchState = MATCH_STATE_PREPARATION;
	Reset();
}

//-----------------------------------------------------------------------------
// Purpose: 
//-----------------------------------------------------------------------------
void CHudMatchState::Reset( )
{	
}

//-----------------------------------------------------------------------------
// Purpose: 
//-----------------------------------------------------------------------------
void CHudMatchState::VidInit( )
{
	Reset();
}

//-----------------------------------------------------------------------------
// Purpose: 
//-----------------------------------------------------------------------------
void CHudMatchState::OnThink( )
{
}

void CHudMatchState::MsgFunc_HudTime( bf_read &msg )
{
	long iTime = msg.ReadLong();
	iTime;
	m_iMatchState = msg.ReadByte();
}

void CHudMatchState::Paint( )
{
	surface()->DrawSetTextFont( m_hTextFont );
	cHL2MPTeamColors[TEAM_SPECTATOR];
	color32 teamColor = cHL2MPTeamColors[TEAM_SPECTATOR];
	surface()->DrawSetTextColor( teamColor.r, teamColor.g, teamColor.b, teamColor.a);

	int x, y, wide, tall;
	this->GetBounds( x, y, wide, tall );

	

	wchar_t unicode[64];
	switch ( m_iMatchState )
	{
	case MATCH_STATE_PREPARATION:
		V_snwprintf( unicode, ARRAYSIZE( unicode ), L"PREPARATION" );
		break;
	case MATCH_STATE_PLAYING:
		V_snwprintf( unicode, ARRAYSIZE( unicode ), L"" );
		break;
	case MATCH_STATE_OVERTAKE:
		V_snwprintf( unicode, ARRAYSIZE( unicode ), L"OVERTAKE" );
		break;
	case MATCH_STATE_OVER:
		V_snwprintf( unicode, ARRAYSIZE( unicode ), L"GAME OVER" );
		break;
	case MATCH_STATE_WINNING_GOAL:
		V_snwprintf( unicode, ARRAYSIZE( unicode ), L"WINNING GOAL" );
		break;
	default:
		V_snwprintf( unicode, ARRAYSIZE( unicode ), L"" );
		break;
	}

	int textWidth = UTIL_ComputeStringWidth( m_hTextFont, unicode );
	surface()->DrawSetTextPos( wide/2 - textWidth/2, 2 );	

	surface()->DrawPrintText( unicode, wcslen( unicode ) );
}