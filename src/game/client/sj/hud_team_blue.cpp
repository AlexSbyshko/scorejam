//========= Copyright Valve Corporation, All rights reserved. ============//
//
// Purpose: 
//
// $NoKeywords: $
//
//=============================================================================//
//
// Health.cpp
//
// implementation of CHudHealth class
//
#include "cbase.h"
#include "hud.h"
#include "hud_team_blue.h"
#include "hud_macros.h"
#include "view.h"

#include "iclientmode.h"

#include <KeyValues.h>
#include <vgui/ISurface.h>
#include <vgui/ISystem.h>
#include <vgui_controls/AnimationController.h>

#include <vgui/ILocalize.h>

using namespace vgui;

#include "hudelement.h"
#include "hud_basetimer.h"
#include "hl2mp_gamerules.h"
#include "c_playerresource.h"

#include "convar.h"

// memdbgon must be the last include file in a .cpp file!!!
#include "tier0/memdbgon.h"

	

DECLARE_HUDELEMENT( CHudTeamBlue );

//-----------------------------------------------------------------------------
// Purpose: Constructor
//-----------------------------------------------------------------------------
CHudTeamBlue::CHudTeamBlue( const char *pElementName ) : CHudElement( pElementName ), vgui::Panel( NULL, "HudTeamBlue" )
{
	vgui::Panel *pParent = g_pClientMode->GetViewport( );
	SetParent( pParent );
}

//-----------------------------------------------------------------------------
// Purpose: 
//-----------------------------------------------------------------------------
void CHudTeamBlue::Init( )
{
	Reset();
}

//-----------------------------------------------------------------------------
// Purpose: 
//-----------------------------------------------------------------------------
void CHudTeamBlue::Reset( )
{	
}

//-----------------------------------------------------------------------------
// Purpose: 
//-----------------------------------------------------------------------------
void CHudTeamBlue::VidInit( )
{
	Reset();
}

//-----------------------------------------------------------------------------
// Purpose: 
//-----------------------------------------------------------------------------
void CHudTeamBlue::OnThink( )
{
}

void CHudTeamBlue::Paint( )
{
	if ( g_PR == NULL )
	{
		return;
	}
	surface()->DrawSetTextFont( m_hTextFont );
	Color color = g_PR->GetTeamColor( TEAM_BLUE );
	surface( )->DrawSetTextColor( color );

	int x, y, wide, tall;
	this->GetBounds( x, y, wide, tall );

	const char *tempName = g_PR->GetTeamName( TEAM_BLUE );
	int textWidth = UTIL_ComputeStringWidth( m_hTextFont, tempName );

	surface()->DrawSetTextPos( wide - textWidth - 3, 2 );

	wchar_t teamNameUnicode[MAX_TEAMNAME_LENGTH];
	
	g_pVGuiLocalize->ConvertANSIToUnicode( tempName, teamNameUnicode, sizeof(teamNameUnicode) );
	surface()->DrawPrintText( teamNameUnicode, wcslen( teamNameUnicode ) );
}