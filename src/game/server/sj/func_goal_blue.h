class CFuncGoalBlueEntity : public CBaseTrigger
{
public:
	DECLARE_CLASS( CFuncGoalBlueEntity, CBaseTrigger );
	DECLARE_DATADESC();
 
	void Spawn();
 
	void Touch( CBaseEntity *pOther );
};
