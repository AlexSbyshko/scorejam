//========= Copyright Valve Corporation, All rights reserved. ============//
//
// Purpose: 
//
// $NoKeywords: $
//
//=============================================================================//
//
// Health.cpp
//
// implementation of CHudHealth class
//
#include "cbase.h"
#include "hud.h"
#include "hud_macros.h"
#include "view.h"

#include "iclientmode.h"

#include <KeyValues.h>
#include <vgui/ISurface.h>
#include <vgui/ISystem.h>
#include <vgui_controls/AnimationController.h>

#include <vgui/ILocalize.h>

using namespace vgui;

#include "hudelement.h"
#include "hud_basetimer.h"
#include "hl2mp_gamerules.h"
#include "c_playerresource.h"

#include "convar.h"

// memdbgon must be the last include file in a .cpp file!!!
#include "tier0/memdbgon.h"

//-----------------------------------------------------------------------------
// Purpose: Health panel
//-----------------------------------------------------------------------------
class CHudTime : public CHudElement, public vgui::Panel
{
	DECLARE_CLASS_SIMPLE( CHudTime, vgui::Panel );

public:
	CHudTime( const char *pElementName );
	virtual void Init( void );
	virtual void VidInit( void );
	virtual void Reset( void );
	virtual void OnThink();
			void MsgFunc_HudTime( bf_read &msg );

protected:
	virtual void	Paint( );

private:
	CPanelAnimationVar( vgui::HFont, m_hTextFont, "TextFont", "HudNumbers" );
	int m_iMatchState;
	int m_iTime;
};	

DECLARE_HUDELEMENT( CHudTime );
DECLARE_HUD_MESSAGE( CHudTime, HudTime );

//-----------------------------------------------------------------------------
// Purpose: Constructor
//-----------------------------------------------------------------------------
CHudTime::CHudTime( const char *pElementName ) : CHudElement( pElementName ), vgui::Panel( NULL, "HudTime" )
{
	vgui::Panel *pParent = g_pClientMode->GetViewport( );
	SetParent( pParent );
	m_iTime = 0;
	m_iMatchState = MATCH_STATE_PREPARATION;
}

//-----------------------------------------------------------------------------
// Purpose: 
//-----------------------------------------------------------------------------
void CHudTime::Init( )
{
	HOOK_HUD_MESSAGE( CHudTime, HudTime );
	Reset();
}

//-----------------------------------------------------------------------------
// Purpose: 
//-----------------------------------------------------------------------------
void CHudTime::Reset( )
{
}

//-----------------------------------------------------------------------------
// Purpose: 
//-----------------------------------------------------------------------------
void CHudTime::VidInit( )
{
	Reset();
}

//-----------------------------------------------------------------------------
// Purpose: 
//-----------------------------------------------------------------------------
void CHudTime::OnThink( )
{
}

void CHudTime::MsgFunc_HudTime( bf_read &msg )
{
	m_iTime = msg.ReadLong( );
	m_iMatchState = msg.ReadByte();
}

void CHudTime::Paint()
{
	if ( g_PR == NULL )
	{
		return;
	}
	surface( )->DrawSetTextFont( m_hTextFont );

	bool isMinutesAndSeconds = true;
	color32 teamColor;
	switch ( m_iMatchState )
	{
	case MATCH_STATE_PLAYING:

		surface( )->DrawSetTextColor( Color( 255, 255, 255, 200 ) );

		break;

	case MATCH_STATE_OVERTAKE:
		
		if ( g_PR->GetTeamScore( TEAM_BLUE ) > g_PR->GetTeamScore( TEAM_RED ) )
		{
			teamColor = cHL2MPTeamColors[TEAM_BLUE];
		}
		else
		{
			teamColor = cHL2MPTeamColors[TEAM_RED];
		}
		surface( )->DrawSetTextColor( teamColor.r, teamColor.g, teamColor.b, teamColor.a );
		break;

	case MATCH_STATE_WINNING_GOAL:

		teamColor = cHL2MPTeamColors[TEAM_SPECTATOR];
		surface( )->DrawSetTextColor( teamColor.r, teamColor.g, teamColor.b, teamColor.a );
		break;

	default:
		isMinutesAndSeconds = false;
		surface( )->DrawSetTextColor( Color( 200, 200, 200, 150 ) );
		break;
	}

	int x, y, wide, tall;
	this->GetBounds( x, y, wide, tall );

	wchar_t unicode[6];
	if ( isMinutesAndSeconds )
	{
		V_snwprintf( unicode, ARRAYSIZE( unicode ), L"%d:%.2d", m_iTime / 60, m_iTime % 60 );
	}
	else
	{
		V_snwprintf( unicode, ARRAYSIZE( unicode ), L"%d", m_iTime );
	}	

	int textWidth = UTIL_ComputeStringWidth( m_hTextFont, unicode );
	surface( )->DrawSetTextPos( wide / 2 - textWidth / 2, 2 );

	surface( )->DrawPrintText( unicode, wcslen( unicode ) );
}