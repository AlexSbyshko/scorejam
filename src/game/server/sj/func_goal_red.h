class CFuncGoalRedEntity : public CBaseTrigger
{
public:
	DECLARE_CLASS( CFuncGoalRedEntity, CBaseTrigger );
	DECLARE_DATADESC();
 
	void Spawn();
 
	void Touch( CBaseEntity *pOther );
};
