#include "cbase.h"
#include "triggers.h"
#include "func_goal_blue.h"
#include "hl2mp_gamerules.h"

 
LINK_ENTITY_TO_CLASS( func_goal_blue, CFuncGoalBlueEntity );
LINK_ENTITY_TO_CLASS( func_goal_team1, CFuncGoalBlueEntity );
 
BEGIN_DATADESC( CFuncGoalBlueEntity )
 
	DEFINE_ENTITYFUNC( Touch ),
 
END_DATADESC()

void CFuncGoalBlueEntity::Spawn()
{
	SetTouch( &CFuncGoalBlueEntity::Touch ); 
	SetSolid( SOLID_VPHYSICS );
	SetMoveType( MOVETYPE_NONE );
	SetModel( STRING( GetModelName() ) );
}

void CFuncGoalBlueEntity::Touch( CBaseEntity *pOther )
{
	if (IsBall(pOther))
	{
		HL2MPRules()->CheckForRedTeamScored();
	}
}