class CFuncOutEntity : public CBaseEntity
{
public:
	DECLARE_CLASS( CFuncOutEntity, CBaseTrigger );
	DECLARE_DATADESC();
 
	void Spawn();
 
	void Touch( CBaseEntity *pOther );
};
