#include "cbase.h"
#include "sj_gamerules.h"

#ifdef CLIENT_DLL
#include "c_hl2mp_player.h"
#else

#include "sj_player.h"
#include "team.h"

#endif

REGISTER_GAMERULES_CLASS( CSJRules );

BEGIN_NETWORK_TABLE_NOBASE( CSJRules, DT_SJRules )

#ifdef CLIENT_DLL
#else
#endif

END_NETWORK_TABLE()

LINK_ENTITY_TO_CLASS( sj_gamerules, CSJGameRulesProxy );
IMPLEMENT_NETWORKCLASS_ALIASED( SJGameRulesProxy, DT_SJGameRulesProxy )

#ifdef CLIENT_DLL
void RecvProxy_SJRules( const RecvProp *pProp, void **pOut, void *pData, int objectID )
{
	CSJRules *pRules = SJRules();
	Assert( pRules );
	*pOut = pRules;
}

BEGIN_RECV_TABLE( CSJGameRulesProxy, DT_SJGameRulesProxy )
RecvPropDataTable( "sj_gamerules_data", 0, 0, &REFERENCE_RECV_TABLE( DT_SJRules ), RecvProxy_SJRules )
END_RECV_TABLE()
#else
void* SendProxy_SJRules( const SendProp *pProp, const void *pStructBase, const void *pData, CSendProxyRecipients *pRecipients, int objectID )
{
	CSJRules *pRules = SJRules();
	Assert( pRules );
	return pRules;
}

BEGIN_SEND_TABLE( CSJGameRulesProxy, DT_SJGameRulesProxy )
SendPropDataTable( "sj_gamerules_data", 0, &REFERENCE_SEND_TABLE( DT_SJRules ), SendProxy_SJRules )
END_SEND_TABLE()
#endif


color32 cSJTeamColors[] =
{
	{ 128, 10, 128, 255 },
	{ 200, 100, 200, 255 },
	{ 153, 204, 255, 255 }, // BLUE
	{ 255, 64, 64, 255 }, // RED
};

char *sSJTeamNames[] =
{
	"Unassigned",
	"Spectator",
	"BARSIKS",
	"MURZIKS",
};


#ifndef CLIENT_DLL

void Command_StartMatch();
ConCommand cc_StartMatch( "sj_match_start", Command_StartMatch, "Start SJ match." );

#endif

CSJRules::CSJRules()
{
#ifndef CLIENT_DLL
	// Create the team managers
	for ( int i = 0; i < ARRAYSIZE( sSJTeamNames ); i++ )
	{
		CTeam *pTeam = static_cast<CTeam*>(CreateEntityByName( "team_manager" ));
		pTeam->Init( sSJTeamNames[i], i );

		color32 color = cSJTeamColors[i];
		pTeam->SetRenderColor( color.r, color.g, color.b, color.a );

		g_Teams.AddToTail( pTeam );

		m_flMatchStartTime = 0;
	}
#endif
}

CSJRules::~CSJRules( void )
{
#ifndef CLIENT_DLL
	// Note, don't delete each team since they are in the gEntList and will 
	// automatically be deleted from there, instead.
	g_Teams.Purge();
#endif
}

void CSJRules::Think( void )
{

}

#ifndef CLIENT_DLL

void CSJRules::StartMatch( void )
{
	UTIL_SayTextAll( "match start!" );

	m_flMatchStartTime = gpGlobals->curtime;
	if ( !IsFinite( m_flMatchStartTime.Get() ) )
	{
		Warning( "Trying to set a NaN game start time\n" );
		m_flMatchStartTime.GetForModify() = 0.0f;
	}

	SpawnBall( );
}

void Command_StartMatch( void )
{
	SJRules()->StartMatch();
}

void CSJRules::SpawnBall( void )
{
	CBaseEntity *pSpot = NULL;
	pSpot = gEntList.FindEntityByClassname( NULL, "info_ball" );

	if ( pSpot )
	{
		CSJBallEntity *pBall = FindBall();
		if ( !pBall )
		{
			pBall = (CSJBallEntity *)CreateEntityByName( "ball" );
			pBall->Spawn();
		}

		pBall->Clear();
		pBall->SetAbsVelocity( Vector( 0, 0, 1 ) );
		pBall->SetAbsOrigin( pSpot->GetAbsOrigin() );
	}
	else
	{
		Warning( "ball spawn not found!" );
	}
}

bool CSJRules::IsPlayerInOffside( CSJ_Player *pPlayer )
{
	return false;
}

void CSJRules::OnPlayerKickedBall( CSJ_Player *pPlayer )
{

}

#endif