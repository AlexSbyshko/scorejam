//========= Copyright Valve Corporation, All rights reserved. ============//
//
// Purpose: 
//
// $NoKeywords: $
//
//=============================================================================//
//
// Health.cpp
//
// implementation of CHudHealth class
//
#include "cbase.h"
#include "hud.h"
#include "hud_macros.h"
#include "view.h"

#include "iclientmode.h"

#include <KeyValues.h>
#include <vgui/ISurface.h>
#include <vgui/ISystem.h>
#include <vgui_controls/AnimationController.h>

#include <vgui/ILocalize.h>

using namespace vgui;

#include "hudelement.h"
#include "hud_basetimer.h"
#include "hl2mp_gamerules.h"
#include "c_playerresource.h"

#include "convar.h"

// memdbgon must be the last include file in a .cpp file!!!
#include "tier0/memdbgon.h"

//-----------------------------------------------------------------------------
// Purpose: Health panel
//-----------------------------------------------------------------------------
class CHudScoreRed : public CHudElement, public vgui::Panel
{
	DECLARE_CLASS_SIMPLE( CHudScoreRed, vgui::Panel );

public:
	CHudScoreRed( const char *pElementName );
	virtual void Init( void );
	virtual void VidInit( void );
	virtual void Reset( void );
	virtual void OnThink();

protected:
	virtual void	Paint( );

private:
	CPanelAnimationVar( vgui::HFont, m_hTextFont, "TextFont", "HudNumbers" );
	int m_iScore;
};	

DECLARE_HUDELEMENT( CHudScoreRed );

//-----------------------------------------------------------------------------
// Purpose: Constructor
//-----------------------------------------------------------------------------
CHudScoreRed::CHudScoreRed( const char *pElementName ) : CHudElement( pElementName ), BaseClass( NULL, "HudScoreRed" )
{
	vgui::Panel *pParent = g_pClientMode->GetViewport( );
	SetParent( pParent );
	m_iScore = 0;
}

//-----------------------------------------------------------------------------
// Purpose: 
//-----------------------------------------------------------------------------
void CHudScoreRed::Init( )
{
	Reset();
}

//-----------------------------------------------------------------------------
// Purpose: 
//-----------------------------------------------------------------------------
void CHudScoreRed::Reset( )
{
}

//-----------------------------------------------------------------------------
// Purpose: 
//-----------------------------------------------------------------------------
void CHudScoreRed::VidInit( )
{
	Reset();
}

//-----------------------------------------------------------------------------
// Purpose: 
//-----------------------------------------------------------------------------
void CHudScoreRed::OnThink( )
{
	if ( g_PR != NULL )
	{
		m_iScore = g_PR->GetTeamScore( TEAM_RED );;
	}	
}

void CHudScoreRed::Paint( )
{
	if ( g_PR == NULL )
	{
		return;
	}
	surface( )->DrawSetTextFont( m_hTextFont );
	surface( )->DrawSetTextColor( g_PR->GetTeamColor( TEAM_RED ) );

	int x, y, wide, tall;
	this->GetBounds( x, y, wide, tall );



	wchar_t unicode[6];
	V_snwprintf( unicode, ARRAYSIZE( unicode ), L"%d", m_iScore );

	int textWidth = UTIL_ComputeStringWidth( m_hTextFont, unicode );
	surface( )->DrawSetTextPos( wide / 2 - textWidth / 2, 2 );




	surface( )->DrawPrintText( unicode, wcslen( unicode ) );
}