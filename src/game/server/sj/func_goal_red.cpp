#include "cbase.h"
#include "triggers.h"
#include "func_goal_red.h"
#include "hl2mp_gamerules.h"

 
LINK_ENTITY_TO_CLASS( func_goal_red, CFuncGoalRedEntity );
LINK_ENTITY_TO_CLASS( func_goal_team2, CFuncGoalRedEntity );

BEGIN_DATADESC( CFuncGoalRedEntity )
 
	DEFINE_ENTITYFUNC( Touch ),
 
END_DATADESC()

void CFuncGoalRedEntity::Spawn()
{
	SetTouch( &CFuncGoalRedEntity::Touch ); 
	SetSolid( SOLID_VPHYSICS );
	SetMoveType( MOVETYPE_NONE );
	SetModel( STRING( GetModelName() ) );
}

void CFuncGoalRedEntity::Touch( CBaseEntity *pOther )
{
	if (IsBall(pOther))
	{
		HL2MPRules()->CheckForBlueTeamScored();
	}
}