#include "cbase.h"
#include "sj_ball.h"
#include "sj_player.h"
#include "sj_gamerules.h"
#include "te_effect_dispatch.h"
#include "baseparticleentity.h"
#include "particle_parse.h"

const char *PROP_COMBINE_BALL_SPRITE_TRAIL = "sprites/laserbeam.vmt";
const char *BALL_MODEL = "models/combine_helicopter/helicopter_bomb01.mdl";
const char *EXPLOSION_PARTICLE_SYSTEM = "explosion_huge";

const int CURVE_COUNT = 6;
const int CURVE_ANGLE = 15;
const int CURVE_TIME = 0.2;
const int DIRECTIONS = 2;
const float ANGLE_DIVIDE = 6.0;

static const char *BALL_GLOW_SPRITES[] =
{
	"sprites/combineball_glow_black_1.vmt",
	"sprites/combineball_glow_black_1.vmt",
	"sprites/combineball_glow_blue_1.vmt", // BLUE
	"sprites/combineball_glow_red_1.vmt", // RED
};

LINK_ENTITY_TO_CLASS( ball, CSJBallEntity );
LINK_ENTITY_TO_CLASS( info_ball, CPointEntity );

// Start of our data description for the class
BEGIN_DATADESC( CSJBallEntity )
 
	// Save/restore our active state
	DEFINE_FIELD( m_bFree, FIELD_BOOLEAN ),
	DEFINE_FIELD( m_bActive, FIELD_BOOLEAN ),
	DEFINE_FIELD( m_flNextChangeTime, FIELD_TIME ),
 
	DEFINE_THINKFUNC( MoveThink ),
	DEFINE_ENTITYFUNC( SJBallTouch ),
 
END_DATADESC()

//-----------------------------------------------------------------------------
// Purpose: Precache assets used by the entity
//-----------------------------------------------------------------------------
void CSJBallEntity::Precache( void )
{
	BaseClass::Precache();
	PrecacheModel( BALL_MODEL ); 
	PrecacheModel( PROP_COMBINE_BALL_SPRITE_TRAIL );

	for ( int i = 0; i < ARRAYSIZE( BALL_GLOW_SPRITES ); i++ )
	{
		PrecacheModel( BALL_GLOW_SPRITES[i] );
	}
	PrecacheParticleSystem( EXPLOSION_PARTICLE_SYSTEM );
	
	PrecacheScriptSound( "NPC_CombineBall.Explosion" );
	PrecacheScriptSound( "d3_citadel.razortrain_horn" );
}

//-----------------------------------------------------------------------------
// Purpose: Sets up the entity's initial state
//-----------------------------------------------------------------------------
void CSJBallEntity::Spawn( void )
{
	Precache();

	//IPhysicsObject *PhysSphereCreate( CBaseEntity *pEntity, float radius, const Vector &origin, solid_t &solid );
	//IPhysicsObject *pSphere = PhysSphereCreate( this, 12, const Vector &origin, solid_t &solid );
	//VPhysicsInitNormal(SOLID_VPHYSICS, 0, false);

	SetThink( &CSJBallEntity::MoveThink );
	SetNextThink( gpGlobals->curtime + 0.05f );
 
	SetModel( BALL_MODEL );
	SetCollisionGroup( COLLISION_GROUP_PROJECTILE );	
	//SetCollisionGroup( COLLISION_GROUP_DEBRIS_TRIGGER );
	SetSolid( SOLID_BBOX ); 
	SetSolidFlags( FSOLID_NOT_SOLID | FSOLID_TRIGGER );

	m_pGlow = CSprite::SpriteCreate( BALL_GLOW_SPRITES[TEAM_UNASSIGNED], GetLocalOrigin( ), false );

	if ( m_pGlow != NULL )
	{
		m_pGlow->FollowEntity( this );
		m_pGlow->SetTransparency( kRenderTransColor, 255, 255, 255, 255, kRenderFxNone );
		//m_pGlow->SetScale( 5.0f );
		//m_pGlow->SetGlowProxySize( 4.0f );
	}

	m_pGlowTrail = CSpriteTrail::SpriteTrailCreate(PROP_COMBINE_BALL_SPRITE_TRAIL, GetAbsOrigin(), false);
	if (m_pGlowTrail != NULL)
	{
		m_pGlowTrail->FollowEntity(this);
		m_pGlowTrail->SetTransparency( kRenderTransColor, 255, 255, 255, 255, kRenderFxNone );
		m_pGlowTrail->SetStartWidth( 24 );
		m_pGlowTrail->SetEndWidth( 12 );
		m_pGlowTrail->SetLifeTime( 1.0f );
		//m_pGlowTrail->TurnOff();
	}

	SetBounceSound("weapons/physcannon/energy_bounce2.wav");

	Clear();
	
	
	//AddSolidFlags( FSOLID_NOT_SOLID );
	//SetSolid( SOLID_VPHYSICS );

	SetTouch( &CSJBallEntity::SJBallTouch );
	
	SetElasticity( 4.0f );

	//m_pOwnerPlayer = NULL;
	//m_bFree = true;

	//SetFree();

	//UTIL_SetSize( this, -Vector(12,12,12), Vector(12,12,12) );

	//SetGravity( UTIL_ScaleForGravity( 400 ) );	// use a lower gravity for grenades to make them easier to see
	//SetFriction( 0.8 );
	//SetSequence( 0 );
	//SetThink(&CSJBallEntity::MoveThink);
}

void CSJBallEntity::Clear( void )
{
	m_iCurvesRemain = 0;
	m_nextCurveTime = 0;
	m_vecSpinDirection = QAngle( 0, 0, 0 );
	m_iDirection = 0;
	m_bBlown = false;
	SetOwnerPlayer( NULL );
	SetFree();	
	ChangeTeam( TEAM_UNASSIGNED );
	SetMoveType( MOVETYPE_FLYGRAVITY, MOVECOLLIDE_FLY_BOUNCE );
	RemoveEffects( EF_NODRAW );
	SetSolid( SOLID_BBOX );
}

void CSJBallEntity::ChangeTeam( int iTeamNum )
{
	color32 color = cSJTeamColors[iTeamNum];
	//SetRenderColor( color.r, color.g, color.b, color.a );
	//m_pGlow->SetRenderColor( color.r, color.g, color.b, color.a );
	m_pGlow->SetModel( BALL_GLOW_SPRITES[iTeamNum] );
	m_pGlowTrail->SetRenderColor( color.r, color.g, color.b, color.a );
	BaseClass::ChangeTeam( iTeamNum );
}

//-----------------------------------------------------------------------------
// Purpose: Think function to randomly move the entity
//-----------------------------------------------------------------------------
void CSJBallEntity::MoveThink( void )
{
	//Msg("Think!\n");

	//// See if we should change direction again
	//if ( m_flNextChangeTime < gpGlobals->curtime )
	//{
	//	// Randomly take a new direction and speed
	//	Vector vecNewVelocity = RandomVector( -64.0f, 64.0f );
	//	SetAbsVelocity( vecNewVelocity );
 //
	//	// Randomly change it again within one to three seconds
	//	m_flNextChangeTime = gpGlobals->curtime + random->RandomFloat( 1.0f, 3.0f );
	//}
 //
	// Snap our facing to where we're heading
	/*Vector velFacing = GetAbsVelocity( );
	QAngle angFacing;
	VectorAngles( velFacing, angFacing );
 	SetAbsAngles( angFacing );*/
 
	if ( GetOwnerPlayer() != NULL && !IsFree() )
	{
		TeleportToPlayerFront( GetOwnerPlayer() );
	}

	if ( IsFree() 
		&& m_nextCurveTime != 0
		&& gpGlobals->curtime >= m_nextCurveTime )
	{
		float amt = (m_iDirection * CURVE_ANGLE) / ANGLE_DIVIDE;
		Vector velocity = GetLocalVelocity();

		VectorAngles( velocity, m_vecSpinDirection );
		m_vecSpinDirection[1] = m_vecSpinDirection[1] + amt;
		m_vecSpinDirection[2] = 0.0;

		Vector forwardVector;
		AngleVectors( m_vecSpinDirection, &forwardVector );

		float speed = velocity.Length();
		velocity[0] = forwardVector[0] * speed;
		velocity[1] = forwardVector[1] * speed;

		SetLocalVelocity( velocity );

		m_iCurvesRemain--;
		if ( m_iCurvesRemain > 0 )
		{
			m_nextCurveTime = gpGlobals->curtime + CURVE_TIME;
		}
		else
		{
			m_nextCurveTime = 0;
		}
	}

	SetNextThink( gpGlobals->curtime + 0.05f );
}

void CSJBallEntity::SJBallTouch( CBaseEntity *pOther )
{
	//BaseClass::BounceTouch( pOther );

	/*if (pOther->entindex() > 0)
	Msg("TOUCH! %i\n", pOther->entindex());*/

	if (IsFree() && !m_bBlown && pOther->IsPlayer() )
	{
		//TeleportToPlayerFront((CBasePlayer *)pOther);
		CSJ_Player *pPlayer = ToSJPlayer( pOther );
		//Msg("OWNER!\n %s team: %i", pPlayer->GetPlayerName(), pPlayer->GetTeamNumber() );
		
		if (pPlayer->IsAlive()
			&& (pPlayer->GetTeamNumber() == TEAM_1 || pPlayer->GetTeamNumber() == TEAM_2)
			&& !SJRules()->IsPlayerInOffside( pPlayer ) )
		{
			SetHolderPlayer(pPlayer);
		}
	}

	float speed = GetAbsVelocity().Length();
	if ( !GetFlags() & FL_ONGROUND 
		&& speed > 65
		&& IsFree() )
	{
		EmitSound( "Bounce.Metal" );		
	}
}

void CSJBallEntity::TeleportToPlayerFront( CBasePlayer *pPlayer )
{
	Vector playerOrigin = pPlayer->GetAbsOrigin();
	QAngle playerAngles = pPlayer->EyeAngles();

	vec_t radians = playerAngles[1] * M_PI / 180;

	Vector newOrigin;
	newOrigin[0] = playerOrigin[0] + cos(radians) * 55.0;
	newOrigin[1] = playerOrigin[1] + sin(radians) * 55.0;
	newOrigin[2] = playerOrigin[2] + 15.0;


	SetAbsOrigin( newOrigin );

	Vector velocity( 0, 0, 1 );
	SetAbsVelocity( velocity );
}

void CSJBallEntity::Kick(CBasePlayer *pPlayer, float flSrength, int direction, QAngle spinAngle)
{	
	CSJ_Player *pSJPlayer = ToSJPlayer( pPlayer );
	if (pSJPlayer == NULL || pSJPlayer->IsInterferenceToKick() )
	{
		return;
	}

	SetOwnerPlayer(pPlayer);
	SetFree();

	Vector playerOrigin = pPlayer->GetAbsOrigin();
	QAngle playerAngles = pPlayer->EyeAngles();

	vec_t radians = playerAngles[1] * M_PI / 180;

	Vector newOrigin;
	newOrigin[0] = playerOrigin[0] + cos(radians) * 55.0;
	newOrigin[1] = playerOrigin[1] + sin(radians) * 55.0;
	newOrigin[2] = playerOrigin[2] + 30.0;

	SetAbsOrigin( newOrigin );

	
	Vector forwardVector;
	AngleVectors( playerAngles, &forwardVector );

	Vector kickVelocity;
	for (int i = 0; i < 3; i++)
	{
		kickVelocity[i] = forwardVector[i] * flSrength;
	}

	SetAbsVelocity( kickVelocity );

	m_iDirection = direction;
	m_vecSpinDirection = spinAngle;
	m_nextCurveTime = gpGlobals->curtime + CURVE_TIME * 2;
	m_iCurvesRemain = CURVE_COUNT;

	SJRules()->OnPlayerKickedBall( pSJPlayer );
	pSJPlayer->EmitSound( "d1_canals.citizenpunch_punch_1" );
}


void CSJBallEntity::SetHolderPlayer(CBasePlayer *pPlayer)
{
	SetOwnerPlayer(pPlayer);
	SetNotFree();

	CSJ_Player *pSJPlayer = (CSJ_Player*)pPlayer;
	pSJPlayer->OnBallReceiving();
}

void CSJBallEntity::SetOwnerPlayer(CBasePlayer *pPlayer)
{
	m_pOwnerPlayer = pPlayer;
	if (pPlayer == NULL)
	{
		ChangeTeam( TEAM_UNASSIGNED );
	}
	else
	{
		Msg("Set ball team %i", pPlayer->GetTeamNumber());
		ChangeTeam( pPlayer->GetTeamNumber() );
	}
}

CBasePlayer *CSJBallEntity::GetOwnerPlayer()
{
	return m_pOwnerPlayer;
}

void CSJBallEntity::GoToOpponent()
{
	CBasePlayer *pOwnerPlayer = GetOwnerPlayer();

	if (!pOwnerPlayer)
	{
		return;
	}

	CSJ_Player *pNearestPlayer = NULL;
	float flMinDistanseToPlayer = -1;

	for (int i = 1; i <= gpGlobals->maxClients; i++)
	{
		CSJ_Player *pPlayer = (CSJ_Player*)UTIL_PlayerByIndex( i );

		if ( pPlayer != NULL
			&& pPlayer->IsAlive()
			&& pPlayer->GetTeamNumber() == GetOpponentTeamNumber( pOwnerPlayer->GetTeamNumber() ) )
		{
			float flDistanseToPlayer = (pPlayer->GetAbsOrigin() - GetAbsOrigin()).Length( );

			if ( flMinDistanseToPlayer == -1 || flDistanseToPlayer < flMinDistanseToPlayer )
			{
				flMinDistanseToPlayer = flDistanseToPlayer;
				pNearestPlayer = pPlayer;
			}
		}		
	}

	if (pNearestPlayer)
	{
		SetHolderPlayer( pNearestPlayer );
	}
	else
	{
		CBaseEntity * pSpot = gEntList.FindEntityByClassname(NULL, "info_ball");
		if ( pSpot )
		{
			Clear();
			SetAbsVelocity(Vector(0, 0, 1));
			SetAbsOrigin(pSpot->GetAbsOrigin());
		}
		else
		{
			Warning("ball spawn not found!");
		}
	}
}

void CSJBallEntity::BlowUp()
{
	m_bBlown = true;
	SetOwnerPlayer( NULL );

	//SetSolid( SOLID_NONE );
	SetMoveType( MOVETYPE_NONE );
	AddEffects( EF_NODRAW );
	m_pGlow->TurnOff();
	//UTIL_Remove(this);
	DispatchParticleEffect( EXPLOSION_PARTICLE_SYSTEM, GetAbsOrigin( ), QAngle( 0, 0, 0 ) );

	/*CRecipientFilter filter;
	filter.AddAllPlayers( );

	EmitSound_t explosionParams;
	explosionParams.m_pSoundName = "NPC_CombineBall.Explosion";*/

	EmitSound( "NPC_CombineBall.Explosion" );

	
	/*EmitSound_t sirenParams;
	sirenParams.m_pSoundName = "d3_citadel.razortrain_horn";*/

	//EmitSound( filter, entindex(), sirenParams );
	//EmitAmbientSound( -2, GetAbsOrigin( ), "d3_citadel.razortrain_horn" );
}

bool CSJBallEntity::IsFree()
{
	return m_bFree;
}

void CSJBallEntity::SetFree()
{
	m_bFree = true;
	//SetSolid( SOLID_BBOX );
}

void CSJBallEntity::SetNotFree()
{
	m_bFree = false;
	//SetSolid( SOLID_NONE );
}