//========= Copyright Valve Corporation, All rights reserved. ============//
//
// Purpose: 
//
// $NoKeywords: $
//=============================================================================//
#if !defined( HUD_TEAM_BLUE_H )
#define HUD_TEAM_BLUE_H
#ifdef _WIN32
#pragma once
#endif

#include "hudelement.h"
#include "hud_numericdisplay.h"
#include <vgui_controls/Panel.h>

//-----------------------------------------------------------------------------
// Purpose: Blue Team panel
//-----------------------------------------------------------------------------
class CHudMatchState : public CHudElement, public vgui::Panel
{
	DECLARE_CLASS_SIMPLE( CHudMatchState, vgui::Panel );

public:
	CHudMatchState( const char *pElementName );
	virtual void Init( void );
	virtual void VidInit( void );
	virtual void Reset( void );
	virtual void OnThink( );
			void MsgFunc_HudTime( bf_read &msg );

protected:
	virtual void Paint( );

private:
	CPanelAnimationVar( vgui::HFont, m_hTextFont, "TextFont", "MatchStatus" );
	int m_iMatchState;
};

#endif // HUD_TEAM_BLUE_H
